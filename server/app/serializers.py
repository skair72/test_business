from rest_framework import serializers

from app.models import BusinessHistory


class BusinessSerializer(serializers.ModelSerializer):
    class Meta:
        model = BusinessHistory
        fields = ('name', 'psrn', 'tin', 'is_small', 'created_at')
