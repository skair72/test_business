from django.core.cache import cache
from rest_framework import generics, status
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.response import Response

from app.helpers import get_business_info, BadDataException
from app.models import BusinessHistory
from app.serializers import BusinessSerializer


class CsrfExemptSessionAuthentication(SessionAuthentication):

    def enforce_csrf(self, request):
        return  # To not perform the csrf check previously happening


class BusinessListCreateAPIView(generics.ListCreateAPIView):
    authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)
    queryset = BusinessHistory.objects.all().order_by('-created_at')
    serializer_class = BusinessSerializer

    def post(self, request, *args, **kwargs):
        query = request.data.get('query')
        if query is None:
            return Response({'error': 'You must provide query'}, status=status.HTTP_400_BAD_REQUEST)

        cached_data = cache.get(query)
        if cached_data is not None:
            headers = self.get_success_headers(cached_data)
            return Response(cached_data, status=status.HTTP_200_OK, headers=headers)

        try:
            business_info = get_business_info(query)
        except BadDataException as e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response({'error': str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        serializer = self.get_serializer(data=dict(business_info._asdict()))
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)

        cache.set(query, serializer.data, 60 * 5)

        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_200_OK, headers=headers)
