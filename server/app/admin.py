from django.contrib import admin

from app.models import BusinessHistory


@admin.register(BusinessHistory)
class CommentInWorkAdmin(admin.ModelAdmin):
    list_display = ('psrn', 'tin', 'is_small')
