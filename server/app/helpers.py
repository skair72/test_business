from typing import NamedTuple

import requests


class BadDataException(Exception):
    def __init__(self, *args, **kwargs):
        Exception.__init__(self, *args, **kwargs)


small_business_categories = (2, 3)


class BusinessInfo(NamedTuple):
    psrn: str
    tin: str
    name: str
    is_small: bool


def get_business_info(query: str) -> BusinessInfo:
    data = {
        'query': query,
    }

    response = requests.post('https://rmsp.nalog.ru/search-proc.json', data=data).json()

    data = response.get('data')

    if data is None or len(data) == 0:
        raise BadDataException('Not found')
    if len(data) > 1:
        raise BadDataException('Too many results')

    data = data[0]

    return BusinessInfo(
        psrn=data.get('ogrn'),
        tin=data.get('inn'),
        name=data.get('name_ex'),
        is_small=int(data.get('category')) in small_business_categories
    )
