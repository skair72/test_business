from django.db import models


# TODO: Separate Business and BusinessHistory

class BusinessHistory(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    psrn = models.CharField(max_length=13, verbose_name='ОГРН')
    tin = models.CharField(max_length=12, verbose_name='ИНН')
    name = models.CharField(max_length=200, verbose_name='Название')
    is_small = models.BooleanField(verbose_name='Малое/Среднее предпринимательство')
