### How to run

```shell
./bin/create_envfile.sh
./bin/run.sh
```
Application will be on 127.0.0.1.

Also you need to wait while frontend do build (You must see 403 error)


#### Do that command if you see 500 error
```shell
make restart c=nginx
```
