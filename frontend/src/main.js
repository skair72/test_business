import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import axios from 'axios';

// axios.defaults.baseURL = 'http://127.0.0.1/api/';
axios.defaults.baseURL = '/api/';

Vue.config.productionTip = false;


new Vue({
    vuetify,
    render: h => h(App)
}).$mount('#app');
